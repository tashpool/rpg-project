using System;
using GameDevTV.Utils;
using RPG.Combat;
using RPG.Core;
using RPG.Movement;
using RPG.Attributes;
using UnityEngine;

namespace RPG.Control
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] private float chaseDistance = 5.0f;
        [SerializeField] private float suspicionTime = 3.0f;
        [SerializeField] private float aggro = 5.0f;
        [SerializeField] private PatrolPath patrolPath;
        [SerializeField] private float waypointTolerance = 1.0f;
        [SerializeField] private float waypointDwellTime = 3.0f;
        [SerializeField] private float shoutDistance = 5.0f;
        [Range(0,1)] [SerializeField] private float patrolSpeedFraction = 0.2f;
        
        private Fighter _fighter;
        private Health _health;
        private GameObject _player;
        private Mover _mover;
        private LazyValue<Vector3> _guardPosition;
        private float _timeSinceSawPlayer = Mathf.Infinity;
        private float _timeSinceArrivedAtWaypoint = Mathf.Infinity;
        private float _timeSinceAggravated = Mathf.Infinity;
        private int _currentWaypointIndex = 0;

        private void Awake()
        {
            // Cache this AI's fighter comp
            _fighter = GetComponent<Fighter>();
            // We have SINGLE player in the game, cache non changing
            _player = GameObject.FindWithTag("Player");
            _health = GetComponent<Health>();
            _mover = GetComponent<Mover>();

            _guardPosition = new LazyValue<Vector3>(GetGuardPosition);
        }

        private Vector3 GetGuardPosition()
        {
            return transform.position;
        }

        private void Start()
        {
            _guardPosition.ForceInit();
        }

        private void Update()
        {
            if (_health.IsDead)
            {
                return;
            }
            if (IsAggravated() && _fighter.CanAttack(_player))
            {
                AttackBehaviour();
            }
            else if (_timeSinceSawPlayer < suspicionTime)
            {
                // be suspicious
                SuspicionBehaviour();
            }
            else
            {
                PatrolBehaviour();
            }

            UpdateTimers();
        }

        public void Aggravate()
        {
            // set timer
            _timeSinceAggravated = 0.0f;
        }

        private void UpdateTimers()
        {
            _timeSinceSawPlayer += Time.deltaTime;
            _timeSinceArrivedAtWaypoint += Time.deltaTime;
            _timeSinceAggravated += Time.deltaTime;
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPosition = _guardPosition.value;
            if (patrolPath != null)
            {
                if (AtWaypoint())
                {
                    _timeSinceArrivedAtWaypoint = 0.0f;
                    CycleWaypoint();
                }

                nextPosition = GetCurrentWaypoint();
            }

            if (_timeSinceArrivedAtWaypoint > waypointDwellTime)
            {
                _mover.StartMoveAction(nextPosition, patrolSpeedFraction);   
            }
        }

        private bool AtWaypoint()
        {
            float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
            return distanceToWaypoint < waypointTolerance;
        }

        private void CycleWaypoint()
        {
            _currentWaypointIndex = patrolPath.GetNextIndex(_currentWaypointIndex);
        }
        
        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetWaypoint(_currentWaypointIndex);
        }

        private void SuspicionBehaviour()
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AttackBehaviour()
        {
            _timeSinceSawPlayer = 0.0f;
            _fighter.Attack(_player);

            AggravateNearbyEnemies();
        }

        private void AggravateNearbyEnemies()
        {
            // sphere cast, all enemies 
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, shoutDistance, Vector3.up, 0);
            foreach (var hit in hits)
            {
                AIController ai = hit.collider.GetComponent<AIController>();
                if (ai == null)
                {
                    continue;
                }
                
                ai.Aggravate();
            }
        }

        private bool IsAggravated()
        {
            float distanceToPlayer = Vector3.Distance(_player.transform.position, transform.position);
            // check aggravated timer status
            return distanceToPlayer < chaseDistance || _timeSinceAggravated < aggro;
        }
        
        // Called by Unity
        private void OnDrawGizmosSelected()
        {
            // Set for all calls
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }
    }
}