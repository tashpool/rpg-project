using UnityEngine;
using UnityEngine.Events;

namespace RPG.Combat
{
    public class Weapon : MonoBehaviour
    {

        [SerializeField] private UnityEvent soundOnHit;
        public void OnHit()
        {
            soundOnHit.Invoke();
        }
    }
}